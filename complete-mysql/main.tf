# provider "aws" {
#   region = "eu-west-2"
# }
#

##################################################################
# Data sources to get AWS Backend and remote state
##################################################################
provider "aws" {
  region = "eu-west-1"
}

data "terraform_remote_state" "network" {
  backend = "s3"

  config {
    bucket = "cv-cloud-remote-state"
    key    = "terraform/vpc"
    region = "eu-west-2"
  }
}

terraform {
  backend "s3" {
    bucket = "cv-cloud-remote-state"
    key    = "terraform/rds"
    region = "eu-west-2"
  }
}

##################################################################
# Data sources to get AWS keys
##################################################################
#
# resource "aws_key_pair" "deployer" {
#   key_name   = "deployer-key"
#   public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC1ddz83D7DmbklDSh5jolnqnMy4krBNJr2akV3oW2CBeRaIuWssgESLYKGRqbqP6kflNwbPw/AYzAPGEXl+kK+ED7tVYOPfH8Sg4kXU48yQDTqdq3v2tOB7ahD6zr1HBHg1eJ6ZQh6qQ2ci2/V9+eUjiSp2aFE68HkAoNXoogBmVcVaGI7smY1NizFPB1iVcdmK4qoBMN7Yifmcy6FO8IhLIix/EusZ9x21BNoXb1B9/g3lp4hO6vOQHBED15RCjLv1YWMlGqUqPhquBYHXo5ge3d4/CGUsq82fP2B2gZ0J257gy/BJFqf8hX32vaO9r8Yo48gsN0Xq//rhfxc3th7 bjibodu@crowdvision.co.uk"
# }

##################################################################
# Data sources to get VPC, subnet, security group and AMI details
##################################################################
##############################################################
# Data sources to get VPC, subnets and security group details
##############################################################


# data "aws_vpc" "default" {
#   default = true
# }
#
# data "aws_subnet_ids" "all" {
#   vpc_id = "${data.aws_vpc.default.id}"
# }
#
# data "aws_security_group" "default" {
#   vpc_id = "${data.aws_vpc.default.id}"
#   name   = "default"
# }
###############################################################

##########################

# data "aws_vpc" "default" {
#   default = true
# }

data "aws_subnet_ids" "all" {
    #  vpc_id = "${data.aws_vpc.default.id}"
#   # vpc_id = "${data.terraform_remote_state.network.private_subnets}"
   vpc_id = "${data.terraform_remote_state.network.vpc_id}"
 }

data "aws_security_group" "default" {
   # vpc_id = "${data.aws_vpc.default.id}"
    vpc_id = "${data.terraform_remote_state.network.vpc_id}"
  name   = "default"
}



#####
# DB
#####
module "db" {
  source = "../"

  identifier = "demodb"

  # All available versions: http://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/CHAP_MySQL.html#MySQL.Concepts.VersionMgmt
  engine            = "mysql"
  engine_version    = "5.7.19"
  instance_class    = "db.t2.large"
  allocated_storage = 5
  storage_encrypted = false

  # kms_key_id        = "arm:aws:kms:<region>:<accound id>:key/<kms key id>"
  name     = "demodb"
  username = "user"
  password = "YourPwdShouldBeLongAndSecure!"
  port     = "3306"

   vpc_security_group_ids = ["${data.aws_security_group.default.id}"]

  #  vpc_security_group_ids = ["${data.terraform_remote_state.network.vpc_id}"]


  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window      = "03:00-06:00"

  # disable backups to create DB faster
  backup_retention_period = 0

  tags = {
    Owner       = "user"
    Environment = "dev"
  }

  # DB subnet group
  subnet_ids = ["${data.aws_subnet_ids.all.ids}"]

  # DB parameter group
  family = "mysql5.7"

  # DB option group
  major_engine_version = "5.7"

  # Snapshot name upon DB deletion
  final_snapshot_identifier = "demodb"

  options = [
    {
      option_name = "MARIADB_AUDIT_PLUGIN"

      option_settings = [
        {
          name  = "SERVER_AUDIT_EVENTS"
          value = "CONNECT"
        },
        {
          name  = "SERVER_AUDIT_FILE_ROTATIONS"
          value = "37"
        },
      ]
    },
  ]
}
